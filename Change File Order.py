import os
for x in os.listdir():
    new = x.split(" ")
    if new[0] not in ["Spring", "Winter"]:
        continue
    new[0], new[1] = new[1], new[0]
    new[-1] = ".zip"
    print(new)
    os.rename(x, " ".join(new))