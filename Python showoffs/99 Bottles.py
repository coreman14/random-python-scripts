
def num_bot(number):
    return "bottles" if number != 1 else "bottle"

for number in range(99, 0, -1):
    bottles = num_bot(number)
    print(f"{number} {bottles} of beer on the wall, {number} {bottles} of beer.")
    print(f"Take one down and pass it around, {number-1} {num_bot(number-1)} of beer on the wall.")
    print(" ")
    
print("No more bottles of beer on the wall, no more bottles of beer.")
print("Go to the store and buy some more, 99 bottles of beer on the wall.")