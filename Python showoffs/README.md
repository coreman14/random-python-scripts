# Showoffs

These scripts were mostly remnants of a blog course i did in college. Some are just guides that i put useful info in to have a place to look back at.

List of Important Scripts:

builtin_show(py and ipynb):
	Shows off some of the lesser known builtins such as:
		- bin,hex and octo conversions
		- enumerate (My class had never had to use enumerate in java)
		- max and min
		- reversed
		- zip


classshow(py and ipynb):
	Another one made for the blog. Shows off how use classes in python including:
		- Class variables
		- Deleting of attributes
	This was followed by TypingInPython.py which showed off basic typing

Api Project(Folder):
	For the blog we had to make a final project that showed off what we taught.
	I went with the request's library and methods. Then showed off the data in a python notebook "APINoteBook"
	I then did a second version that used async to grab all the requests at once.

