"""A show off of argparse. Used for reference on new files
"""
import os
import argparse # Package import
parser = argparse.ArgumentParser(description="Outputs a the number given through the command line") # Making the object and writing the help description
parser.add_argument("number", type=float, help="A number to output.")
parser.add_argument("-m", "--multiply", type=float, help="A number to multiply the given number by")
args = parser.parse_args()
args.number = args.number*args.multiply if args.multiply is not None else args.number
if args.number.is_integer():
    print(f"Your number is {int(args.number)}")
else:
    print(f"Your number is {args.number}")


# Additional tips related to argparse

# Check for directory

def dir_path(path: str) -> str:
    """Checks if input is a directory"""
    if os.path.isdir(path):
        return path
    raise argparse.ArgumentTypeError(f"readable_dir:{path} is not a valid path")

parser.add_argument("-o", "--output", type=dir_path)

#pylint: disable=pointless-string-statement
"""
Words of wisdom.

You can subclass ArgumentParser and replace the error function to change how an error outputs.
This is useful for if you want the help message to be ouputted everytime even with no args

use nargs="+" or "*" to compile on to more than one list
-foo "String" "Int"
Namespace(foo=["String", "Int"])


the action append does the same job but only wants one arg at a time
-foo "String" -foo "Int"

To make a argument required but have different options use choices and lower strings:
parser.add_argument('action', type=lambda x: str(x).lower(), choices=['save', 'create'], help='Choose whether to create a direcrtory from a file or save a directory to a file.')

"""
#pylint: enable=pointless-string-statement
#Enable just in case
