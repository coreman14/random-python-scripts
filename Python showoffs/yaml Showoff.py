import yaml
from colorama import Fore as f,Style as s, init as i 
i(convert=True)
yamlin = ""
try:
    with open("character.yml", 'r') as stream:
        try:
            yamlin = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
except Exception:
    print(f.RED+"Error: character.yml file not found, using default values"+s.RESET_ALL)
    yamlin = {'display_name':"Kyle",'outfit':"Monster Energy Shirt"}
print(f"OMG, hi {yamlin['display_name']}, i love your {yamlin['outfit']}!")