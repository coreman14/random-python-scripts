import os

types = ["png", "webp"]

dirname = os.path.abspath(os.path.dirname(__file__))

current_scen = ""
deep = 0
char_name = ""
pose_name = ""

out_path = "outfits"
char_path = "characters"

chars = set()
os.chdir(dirname)

if not os.path.exists(char_path):
    os.mkdir(char_path)

if not os.path.exists(out_path):
    os.mkdir(out_path)

out_path = os.path.abspath(out_path)

for root, dirs, files in os.walk(dirname):
    if "characters" in dirs:
        current_scen = root.split(os.sep)[-1]
    elif "character.yml" in files or "character.json" in files:
        char_name = root.split(os.sep)[-1]
        deep = len(root.split(os.sep))
    elif len(root.split(os.sep)) == deep+1:
        pose_name = root.split(os.sep)[-1]
    elif "outfits" in root.lower().split(os.sep)[-2]:
        outfit_folder = root.split(os.sep)[-1]
        print(current_scen, char_name, pose_name, outfit_folder)
        for f in files:
            if f"{char_name}_{current_scen}" not in chars:
                os.link(root + os.sep + f, char_path + f"{os.sep}{char_name}_{current_scen}_{f}")
                chars.add(f"{char_name}_{current_scen}")
            if any(f.endswith(x) for x in types):
                os.link(root + os.sep + f, out_path + f"{os.sep}{char_name}_{current_scen}_{pose_name}_{outfit_folder}_{f}")

    elif root.endswith("outfits"):
        print(current_scen, char_name, pose_name)
        for f in files:
            if f"{char_name}_{current_scen}" not in chars:
                os.link(root + os.sep + f, char_path + f"{os.sep}{char_name}_{current_scen}_{f}")
                chars.add(f"{char_name}_{current_scen}")
            if any(f.endswith(x) for x in types):
                os.link(root + os.sep + f, out_path + f"{os.sep}{char_name}_{current_scen}_{pose_name}_{f}")
