DROP TABLE IF EXISTS `chars`;
		
CREATE TABLE `chars` (
  `char_name` VARCHAR(256) NOT NULL,
  `scenario_name` VARCHAR(256) NOT NULL,
  `pose_name` VARCHAR(256) NOT NULL,
  `outfit_folder` VARCHAR(256) NOT NULL DEFAULT NULL,
  `outfit_name` VARCHAR(256) NOT NULL,
  PRIMARY KEY (`char_name`, `scenario_name`, `pose_name`, `outfit_folder`, `outfit_name`)
);
