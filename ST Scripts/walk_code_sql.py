import os
import sqlite3

types = ["png", "webp"]

dirname = os.path.abspath(os.path.dirname(__file__))

current_scen = ""
deep = 0
char_name = ""
pose_name = ""

os.chdir(dirname)

conn = sqlite3.connect("char_db.db")

for root, dirs, files in os.walk(dirname):
    if "characters" in dirs:
        current_scen = root.split(os.sep)[-1]
    elif "character.yml" in files or "character.json" in files:
        char_name = root.split(os.sep)[-1]
        deep = len(root.split(os.sep))
    elif len(root.split(os.sep)) == deep+1:
        pose_name = root.split(os.sep)[-1]
    elif "outfits" in root.lower().split(os.sep)[-2]:
        outfit_folder = root.split(os.sep)[-1]
        sql = '''INSERT INTO chars (char_name, scenario_name, pose_name, outfit_folder, outfit_name)
                VALUES (?,?,?,?,?);'''
        cur = conn.cursor()
        for f in files:
            if any(f.endswith(x) for x in types):
                for x in types:
                    f = f.replace("." + x,"")
                cur.execute(sql, (char_name.lower(), current_scen, pose_name.lower(), outfit_folder, f.lower()))
        cur.close()
        conn.commit()

    elif root.endswith("outfits"):
        sql = '''INSERT INTO chars (char_name, scenario_name, pose_name, outfit_folder, outfit_name)
                VALUES (?,?,?,?,?);'''
        cur = conn.cursor()
        for f in files:
            if any(f.endswith(x) for x in types):
                for x in types:
                    f = f.replace("." + x,"")
                print(char_name.lower(), current_scen, pose_name.lower(), "", f.lower())
                cur.execute(sql, (char_name.lower(), current_scen, pose_name.lower(), "", f.lower()))
        cur.close()
        conn.commit()
