DROP TABLE IF EXISTS `chars2`;
		
CREATE TABLE `chars2` (
  `char_name` VARCHAR(256) NOT NULL,
  `scenario_name` VARCHAR(256) NOT NULL,
  `pose_name` VARCHAR(256) NOT NULL,
  `outfit_folder` VARCHAR(256) NULL DEFAULT NULL,
  `outfit_name` VARCHAR(256) NOT NULL,
  PRIMARY KEY (`char_name`, `scenario_name`, `pose_name`, `outfit_folder`, `outfit_name`)
);

insert into chars2 select * from chars;

DROP TABLE IF EXISTS `chars`;
		
CREATE TABLE `chars` (
  `char_name` VARCHAR(256) NOT NULL,
  `scenario_name` VARCHAR(256) NOT NULL,
  `pose_name` VARCHAR(256) NOT NULL,
  `outfit_folder` VARCHAR(256) NULL DEFAULT NULL,
  `outfit_name` VARCHAR(256) NOT NULL,
  PRIMARY KEY (`char_name`, `scenario_name`, `pose_name`, `outfit_folder`, `outfit_name`)
);

insert into chars select * from chars2 order by char_name, scenario_name, pose_name, outfit_folder, outfit_name;

drop table if EXISTS chars2;