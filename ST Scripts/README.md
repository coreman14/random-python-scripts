# Random Python Scripts

These scripts are random ones that I have made that will never be used in a real project. They are small ideas that would've taken less work to do by hand, rather than code for them

List of Scripts:

walk_code/walk_code_sql:
	Scans through folders for characters, then creates either a dump of hard linked folder or an sqlite database with information on characters, outfits and pose names.

char_db.db:
	sqlite database.

orderdb.sql:
	Refills the first table ordered by character name, what scenario, the pose name and outfit name

sqldb.sql:
	Creates the base tables for the sqlite db.