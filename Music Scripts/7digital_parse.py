import os
from colorama import Fore, init

init(autoreset=True, convert=True)
music_dir = r"C:\Users\rcgam\Music\Music"
exclusion_prefix = "7:"
complete_prefix = "c:"
super_exclude_prefix = "x:"
all_prefix = [exclusion_prefix, complete_prefix, super_exclude_prefix]
exclusion = set()
accepted_types = ["mp3", "flac"]
parse_file = "parse_skip.txt"

def skip_write(prefix, writer=None):
    pre = exclusion_prefix if writer is None else writer
    with open(parse_file, "a+", encoding="utf8") as a:
        a.write(pre + prefix +"\n")
    exclusion.add(pre + prefix +"\n")

with open(parse_file, "r+", encoding="utf8") as w:
    exclusion.update(w.readlines())

for outer in os.listdir(music_dir):
    if os.path.isdir(music_dir + os.sep + outer) and all(
        y + outer + "\n" not in exclusion for y in all_prefix
    ):
        print("Folder: "  + Fore.RED + outer)
        print("\n".join(os.listdir(music_dir + os.sep + outer)))
        an = input("Would you like to parse this folder?(y|Y to parse, na to never ask again, anything else to never ask for this parse_type): ")
        if an.lower() != "y":
            skip_write(outer, None if an.lower() != "na" else super_exclude_prefix)
            continue

        print("Parseing "  + Fore.RED + outer)
        for x in os.listdir(music_dir + os.sep + outer):
            if any(x.lower().endswith(y) for y in accepted_types):
                c = x.split(" - ")[2]
                if c[0] in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']:
                    c = c.split(" ")[1:]
                    c = " ".join(c)
                print(c)
                os.rename(music_dir + os.sep + outer + os.sep  + x, music_dir + os.sep + outer + os.sep + c)
        skip_write(outer, complete_prefix)

