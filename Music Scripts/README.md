# Random Python Scripts

These scripts are random ones that I have made that will never be used in a real project. They are small ideas that would've taken less work to do by hand, rather than code for them

List of Scripts:

*_parse.py
	These scripts parse and change file names based on where the file was downloaded from. Since all information is stored in the metadata, it removes Artist, album and number from the filename. It also creates and stores parsed folder names in a txt file.