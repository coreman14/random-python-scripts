Raymond Hettinger's professional at doing code review and architecture review

P vs. NP. Pythonic vs. Non-Pythonic.

## How to make use of PEP 8
1. Golden rule of PEP-8: PEP-8 onto yourself. PEP 8 is style guide, not a law book.
2. Care about intelligibility, not simply visually better 
3. Transforming (Java) API to pythonic ones

## Why not PEP 8
1. Code beautifully PEP 8 compliant but bad
2. Distraction from code quality
3. PEP eightify would wrap history in case of `blame` 

More details can be found in codes comments and talks.