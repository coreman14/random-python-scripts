from collections.abc import Set, MutableSet, MutableMapping
from contextlib import suppress
from operator import itemgetter
import os
import sqlite3
class ListBasedSet(Set):
    '''
    Alternate set implementation favoring space over speed and not requiring the set elements to be hashable.
    '''
    def __init__(self, iterable) -> None:
        self.elements = []
        for value in iterable:
            if value not in self.elements:
                self.elements.append(value)
                
    def __iter__(self):
        return iter(self.elements)
    
    def __contains__(self, x: object) -> bool:
        return x in self.elements
    
    def __len__(self) -> int:
        return len(self.elements)
    
    
class BitSet(MutableSet):
    'Ordered set with compact storage for integers in a fixed range'
    def __init__(self, limit, iterable=()) -> None:
        self.limit = limit
        num_bytes = (limit+7) // 8 #Double slash is forced int division
        self.data = bytearray(num_bytes)
        self |= iterable
        
    def _get_location(self,elem):
        if elem < 0 or elem >= self.limit:
            raise ValueError(f'{elem!r} must be in range <= elem < {self.limit}')
        return divmod(elem, 8)
    
    def __contains__(self, x: object) -> bool:
        bytenum, bitnum = self._get_location(x)
        return bool((self.data[bytenum] >> bitnum) & 1)
    
    def add(self, elem):
        bytenum, bitnum = self._get_location(elem)
        self.data[bytenum] |= (1 << bitnum)
        
    def discard(self, value) -> None:
        bytenum, bitnum = self._get_location(value)
        self.data[bytenum] &= ~(1 << bitnum)
        
    def __iter__(self) :
        for elem in range(self.limit):
            if elem in self:
                yield elem
    
    def __len__(self) -> int:
        return sum(1 for elem in self)
    
    def __repr__(self):
        return f'{type(self).__name__} (limit={self.limit}, iterable ={list(self)})'
    
    def _from_iterable(self, iterable):
        return type(self)(self.limit, iterable)
    
class FileDict(MutableMapping):
    """
    File based dictionary
    a dictionary-like object based on the  file system rather than in-memory hash tables. It is persistent and sharable between processes.
    
    MutableMapping.__abstractmethods__:
    '__iter__', __delitem__', '__setitem__', '__getitem__', '__len__'
    """
    
    def __init__(self, dirname, pairs=(), **kwargs) -> None:
        self.dirname = dirname
        with suppress(FileExistsError):
            os.mkdir(dirname)
        self.update(pairs, **kwargs)
        
    def __getitem__(self, key):
        fullname = os.path.join(self.dirname, key)
        try:
            with open(fullname) as f:
                return f.read()
        except FileNotFoundError:
            raise KeyError(key) from None
    
    def __setitem__(self, key, value):
        fullname = os.path.join(self.dirname, key)
        with open(fullname, "w") as f:
            f.write(value)
            
    def __delitem__(self, key):
        fullname = os.path.join(self.dirname, key)
        try:
            os.remove(fullname)
        except FileNotFoundError:
            raise KeyError(key) from None
        
    def __len__(self):
        return len(os.listdir(self.dirname))
    
    def __iter__(self):
        return iter(os.listdir(self.dirname))
    
    def __repr__(self):
        return f'FileDict{tuple(self.items())}'
    

class SQLDict(MutableMapping):
    """Dictionary-like object with a database back-end store
    
        Concurrent and persistent.
        Easy to share with other programs
        Queryable.
        Single file (easy to email and to backup)
    """
    
    def __init__(self, dbname, items=[], **kwds):
        self.dbname = dbname
        self.conn = sqlite3.connect(dbname)
        c = self.conn.cursor()
        with suppress(sqlite3.OperationalError):
            c.execute('CREATE TABLE Dict (key text, value text)')
            c.execute('CREATE UNIQUE INDEX Kndx On Dict (key)')
        self.update(items, **kwds)
    
    def __setitem__(self, k, v) -> None:
        if k in self:
            del self[k]
        with self.conn as c:
            c.execute('INSERT INTO Dict VALUES (?, ?)', (k, v))
            
    def __getitem__(self, k):
        c = self.conn.execute("SELECT value from Dict where key=?",(k,))
        row = c.fetchone()
        if row is None:
            raise KeyError(k)
        return row[0]
    
    def __delitem__(self, v) -> None:
        if v not in self:
            raise KeyError(v)
        with self.conn as c:
            c.execute('DELETE FROM Dict where key=?', (v,))
            

    def __len__(self) -> int:
        return next(self.conn.execute('select count(*) from Dict'))[0]
    
    def __iter__(self):
        c = self.conn.execute('SELECT key from Dict')
        return map(itemgetter(0), c.fetchall())

    def __repr__(self) -> str:
        return f'{type(self).__name__} (dbname={self.dbname!r}, iterable ={list(self.items())})'
    
    
    def close(self):
        self.conn.close()
    
    
    
    
    
    
    
    
    
    

if __name__ == "__main__":
    #BitSet
    s = BitSet(100, [20, 30, 40, 60, 75])
    print(20 in s)
    t = BitSet(100, [20, 40, 75, 93, 97])
    print(s & t)
    print(s | t)
    print(s - t)
    print(t - s)
    print(s.data)
    print(list(s.data))
    print(bin(16))
    print(bin(64))
    len(s.data) #Very small storage
    
    
    #Filedict
    starks = FileDict('starks')
    starks['arya'] = 'tomboy'
    starks['sansa'] = 'bad taste in men'
    print(starks.keys())
    print(starks.values())
    print(starks['arya'])
    
    #Go into the new directory and change arya to something else
    input("Change the arya file in starks to something else")
    print(starks['arya'])
    
    
    #SqlDict
    starks = SQLDict('starks.db')
    starks['arya'] = 'tomboy'
    starks['sansa'] = 'bad taste in men'
    print(starks.keys())
    print(starks.values())
    print(starks['arya'])
    
    