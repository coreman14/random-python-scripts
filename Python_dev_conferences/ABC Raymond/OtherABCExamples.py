#Pure abstract class
from abc import ABC, abstractmethod
from collections import deque
from sys import intern
import re

class Entertainer(ABC):
    
    @abstractmethod
    def sing(self,sing):
        pass

    @abstractmethod
    def dance(self):
        pass
    
#Framework class
class Puzzle(ABC):
    pos = ""
    goal = ""
    
    def __init__(self, pos=None):
        if pos: self.pos = pos

    def __repr__(self):
        return repr(self.pos)
    
    def canonical(self):
        return repr(self)
    
    def isgoal(self):
        return self.pos == self.goal
    
    @abstractmethod
    def __iter__(self):
        pass

    def solve(self, depthFirst=False):
        queue = deque([self])
        trail = {intern(self.canonical()): None}
        solution = deque()
        load = queue.append if depthFirst else queue.appendleft
        
        while not self.isgoal():
            for m in self:
                c = m.canonical()
                if c in trail:
                    continue
                trail[intern(c)] = self
                load(m)
            self = queue.pop()
        
        while self:
            solution.appendleft(self)
            self = trail[self.canonical()]
            
        return list(solution)
        

class Stepper(Puzzle):
    pos = (1,0,0,0,0,0)
    goal = (0,0,0,0,0,1)
    
    def __iter__(self):
        i = self.pos.index(1)
        if i > 0: yield Stepper((0,) *  (i-1) + (1,) + (0,) * (6-i))
        if i < 6: yield Stepper((0,) *  (i+1) + (1,) + (0,) * (4-i))
        
s = Stepper((1,0,0,0,0,0))
print(s.solve())

class JugFill(Puzzle):
    pos = (0, 0, 8)
    
    capacity = (3, 5, 8)
    
    goal = ()