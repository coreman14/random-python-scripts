Closing Thoughts
================

Challenges
----------

1) Adding ``__slots__`` to a dataclass is currently incompatible
   having default values.  This will be hard to fix without
   having the class decorator create a new class rather than
   modifying the class in place.

2) A special technique, ``__post_init__()`` is needed to call
   a ``__init__()`` in a parent class.

3) Dataclasses currently don't work well with immutable parent
   classes where we need to override or extend a ``__new__()``
   method.

Future directions
-----------------

* Direct support for ``__slots__``
* Custom data validators  
