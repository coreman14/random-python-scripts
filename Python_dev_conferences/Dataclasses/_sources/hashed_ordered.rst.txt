Freezing and Ordering
=====================

For the common case, dataclasses are mutable. That is one of their
principal benefits.   Accordingly, they are not hashable so their
instances cannot be used as set elements or dictionary keys.

Likewise, dataclasses are not orderable by default. The reason for this
restriction is to prevent the TypeErrors that ensue when one or more of the
fields is unorderable.

When needed, these defaults are easily overridden by specifying that you want
immutability, hashability, and ordering.


Creating the dataclass
----------------------

.. literalinclude:: code/hashed_ordered.py


Working with the dataclass
--------------------------

Presto, now we have hashability and ordering!

.. literalinclude:: code/hashed_ordered_session.py


Generated code
--------------

.. literalinclude:: code/hashed_ordered_generated.py

Interesting points
------------------

* Unlike `functools.total_ordering()`, the comparison methods are type
  specific and don't need to worry about reversed fallback comparisons.

* Because `object()` has default comparison methods that return
  *NotImplemented*, the comparison methods do not call `super()`.

* Frozen behavior (immutability) is implemented by extending
  `__setattr__()` and `__delattr__()` to block writes and deleted
  to fields.

* This differs from the usual hand-written approach which uses
  read-only properties.  See the *Fractions* module for an example.

* The extended attribute access methods do call `super()` so that they
  don't unintentionally turn-off important behaviors in a parent class.

* The hash method matches what would be written by hand.

* In named tuples, all of these behaviors come for free because they are
  inherited from tuple.  And being written in C, the named tuple
  versions are much faster than these pure python implementations.
