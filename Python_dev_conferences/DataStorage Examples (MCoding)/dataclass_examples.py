"""This file shows different ways to do data storage in python


EXPLAINATION OF FEATURES:
mutable: Class allows data to be changed after initialization
immutable: Class can stop data from being changed after initialization
*A class can be both mutable and immutable if given the option

slots: Can restrict memory usage by limiting what goes into the class
defaults: Can set default's for variables
default factories: Can make new object's like lists for each object
keyword getset: Can set attributes using ".attri = 0"
converter: Can converter data given to it
validators: Can make sure data is a specific type
typesafe: Respects typing. Will error out if a str is given to an int
standard library: Does not have to be pip installed

"""
# attrs and pydantic are not built in
# pip install pydantic
# pip install attrs
# pylint: disable-all


def plain_class_example():
    """Example of making a class by hand
    A plain class has all features, but must be implemented by hand
    mutable: True
    immutable: manual
    slots: True
    defaults: True
    default factories: True
    keyword getset: True
    converter: manual
    validators: manual
    typesafe: True
    standard library: True
    """
    class PlainClass:
        def __init__(self, n: int, f: float, s: str):
            self.n = n
            self.f = f
            self.s: str = s

    x = PlainClass(42, 4.5, 'hello')
    x = PlainClass(42, f=4.5, s='hello')
    y = x.n
    x.n = 0
    


def dataclass_example():
    """
    #SLOTS ONLY WORK ON PYTHON 3.10
    mutable: True
    immutable: True
    slots: True
    defaults: True
    default factories: True
    keyword getset: True
    converter: False
    validators: False
    typesafe: True
    standard library: True
    """
    from dataclasses import dataclass
    # Slots only work on 3.10
    # @dataclass(slots=True)
    @dataclass()
    class T:
        n: int
        f: float
        s: str

    x = T(42, 4.5, 'hello')
    x = T(24, f=4.5, s='hello')
    y = x.n
    x.n = 0
    


def attr_class_example():
    """
    mutable: True
    immutable: True
    slots: True
    defaults: True
    default factories: True
    keyword getset: True
    converter: True
    validators: True
    typesafe: True
    standard library: False
    """
    import attr

    @attr.s
    class T:
        n: int = attr.ib(converter=int)  # verbose
        f: float = attr.ib(validator=attr.validators.instance_of(float))
        s: str = attr.ib(default="")
        l: list = attr.ib(factory=list)

    x = T(42, 4.5, 'hello')
    x = T(42, f=4.5, s='hello')
    # can convert or validate but not required

    y = x.n
    x.n = 0


def tuple_example():
    """
    mutable: False
    immutable: True
    slots: True
    defaults: False
    default factories: False
    keyword getset: False
    converter: False
    validators: False
    typesafe: False
    standard library: True
    """
    x = 42, 4.5, 'hello'
    y = x[0]  # access by index error-prone
    # immutable


def namedtuple_example():
    """
    mutable: False
    immutable: True
    slots: True
    defaults: True
    default factories: False
    keyword getset: True
    converter: False
    validators: False
    typesafe: False
    standard library: True
    """
    from collections import namedtuple

    T = namedtuple('T', ['n', 'f', 's'])

    x = T(42, 4.5, 'hello')
    x = T(42, f=4.5, s='hello')

    y = x[0]
    y = x.n
    # immutable


def namedtuple_as_class_example():
    """
    mutable: False
    immutable: True
    slots: True
    defaults: True
    default factories: False
    keyword getset: True
    converter: False
    validators: False
    typesafe: True
    standard library: True
    """
    from typing import NamedTuple

    class T(NamedTuple):
        n: int
        f: float
        s: str

    x = T(42, 4.5, 'hello')
    x = T(42, f=4.5, s='hello')

    y = x[0]
    y = x.n
    # immutable




def dict_example():
    """
    mutable: True
    immutable: False
    slots: False
    defaults: False
    default factories: False
    keyword getset: by string
    converter: False
    validators: False
    typesafe: False
    standard library: True
    """
    x = {
        'n': 42,
        'f': 4.5,
        's': 'hello'
    }
    y = x['n']  # access by str error-prone
    x['n'] = 0






def SimpleNameSpace_example():
    """
    mutable: True
    immutable: False
    slots: Slots
    defaults: False
    default factories: False
    keyword getset: True
    converter: False
    validators: False
    typesafe: False
    standard library: True
    
    """
    from types import SimpleNamespace

    x = SimpleNamespace(n=42, f=4.5, s='hello')  # must be kwargs

    class NS:
        pass
    y = NS()

    y = x.n
    x.n = 0









def pydantic_example():
    """
    mutable: True
    immutable: True
    slots: True
    defaults: True
    default factories: True
    keyword getset: True
    converter: True
    validators: True
    typesafe: True
    standard library: False
    """
    from pydantic import BaseModel

    class T(BaseModel):
        n: int
        f: float
        s: str

    x = T(n=42, f=4.5, s='hello')  # must be kwargs
    y = x.n
    x.n = 0
    # args always converted to given types, type-checked at runtime


