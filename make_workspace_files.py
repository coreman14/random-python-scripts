"""Creates new workspaces for Python projects, to avoid terminal issues"""
import os
FILE_EXT = ".code-workspace"
CONFIG_FOLDER = "@CONFIG FILES"
TEMPLATE_FILE = 'workspace.template'
WORKSPACE_FOLDER = '@WORKSPACE FILES'
if not os.path.isdir(CONFIG_FOLDER):
    print(f"The config folder is missing. Make sure folder {CONFIG_FOLDER} exist " +
          "and '{TEMPLATE_FILE}' is inside")
    exit(1)
if not os.path.isfile(CONFIG_FOLDER + os.sep + TEMPLATE_FILE):
    print(F"{TEMPLATE_FILE} is missing from {CONFIG_FOLDER}. Please make sure it exists's")
with open(CONFIG_FOLDER + os.sep + TEMPLATE_FILE, "r", encoding="utf-8") as temp:
    replacement_text = temp.read()

fold_list = os.listdir()
fold_list.remove(CONFIG_FOLDER)
fold_list.remove(WORKSPACE_FOLDER)
for folder in fold_list:
    if (os.path.isdir(folder) and not os.path.isfile(WORKSPACE_FOLDER + os.sep + folder + FILE_EXT)):
        print(f"New file {folder + FILE_EXT}")
        with open(WORKSPACE_FOLDER + os.sep + folder + FILE_EXT, "w+", encoding="utf-8") as new_wo:
            new_wo.write(replacement_text.replace("/\\", "../"  +folder))



"""
JUST IN CASE HERES THE FILE


'
{
	"folders": [
		{
			"path": "/\"
		}
	],
	"settings": {}
}

'
"""